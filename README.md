# ForestKeeper Reporter #

Generates notable facts about the raw data from member activities, tree observations, and mast surveys

### How do I use it? ###

* Generate "Raw Data- Activity Reports" for the time period you desire
* click the save icon and save as csv
* Do the same for "Raw Data- Mast Survey Record" and "Raw Data - Tree Observations"
* Download and unzip this project anywhere on your computer
* Double-click "forestKeeperReporter.html" to open it in your browser (preferably the lastest version of Chrome)
* Upload the files you downloaded in the first three steps and then you'll see the statistics